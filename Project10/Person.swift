//
//  Person.swift
//  Project10
//
//  Created by Danni Brito on 12/12/19.
//  Copyright © 2019 Danni Brito. All rights reserved.
//

import UIKit

class Person: NSObject, Codable {
    var name: String
    var image: String
    
    init(name: String, image: String) {
        self.name = name
        self.image = image
    }

}
